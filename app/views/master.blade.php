<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Map</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/styles.css')}}">
	<script src="{{asset('js/jquery.js')}}"></script>
	
	

	@yield('style')
</head>
<body>
<div class=" text-center container">
	
	@yield('content')
</div>
	

	

	
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSAP63ALlaO62CE4QEPDTE-ggIZEgJX-U&libraries=places"
    async defer></script>
    	<script src="{{asset('js/script.js')}}"></script>

	@yield('script')
</body>
</html>